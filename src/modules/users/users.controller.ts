import { Body, Controller, Post, UseInterceptors } from '@nestjs/common';
import { UsersService } from './users.service';
import { TransformInterceptor } from 'src/interceptors/transform.interceptor';
import { ApiTags } from '@nestjs/swagger';
import { UserRegisterDto } from './dto/create-user.dto';

@ApiTags('users')
@Controller('users')
@UseInterceptors(new TransformInterceptor())
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async register(@Body() userRegisterDto: UserRegisterDto) {
    return this.usersService.saveUser(userRegisterDto);
  }
}
