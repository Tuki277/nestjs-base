import { ConflictException, Injectable } from '@nestjs/common';
import { UserRegisterDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';
import { UserRepository } from './user.repository';
import { USER_EXISTS } from './const';
import { hashPassword } from 'src/helpers/hashPassword';

@Injectable()
export class UsersService {
  constructor(private readonly userRepository: UserRepository) {}

  async saveUser(req: UserRegisterDto): Promise<User | undefined> {
    const user = await this.findByEmail(req.email);
    if (user) {
      throw new ConflictException(USER_EXISTS);
    }

    req.roles = ['BASE'];
    req.servicePack = ['FREE'];
    req.password = await hashPassword(req.password);
    const userData = this.userRepository.create(req);
    return this.userRepository.save(userData);
  }

  findByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOne({
      where: { email: email },
    });
  }
}
