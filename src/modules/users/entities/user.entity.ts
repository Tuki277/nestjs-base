import { AppRoles } from 'src/common/enum';
import { Todo } from 'src/modules/todo/entities/todo.entity';
import {
  Entity,
  Column,
  BaseEntity,
  PrimaryGeneratedColumn,
  OneToOne,
} from 'typeorm';

@Entity({ name: 'users' })
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ length: 50, unique: true })
  email: string;

  @Column({ length: 255 })
  username: string;

  @Column()
  password: string;

  @Column({ nullable: true, name: 'first_name' })
  firstName: string;

  @Column({ nullable: true, name: 'last_name' })
  lastName: string;

  @Column('int', { nullable: true })
  age: number;

  @Column({
    default: false,
    name: 'is_active',
  })
  isActive: boolean;

  @Column({
    nullable: true,
    type: 'text',
    enum: AppRoles,
    array: true,
  })
  roles?: string[];

  @Column({
    nullable: true,
    type: 'text',
    array: true,
    name: 'service_pack',
  })
  servicePack: string[];

  @OneToOne((type) => Todo, (todo) => todo.id)
  todos: Todo[];
}
