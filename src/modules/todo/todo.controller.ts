import { Body, Controller, Get, Post, UseInterceptors } from '@nestjs/common';
import { TodoService } from './todo.service';
import { ApiTags } from '@nestjs/swagger';
import { TransformInterceptor } from 'src/interceptors/transform.interceptor';
import { CreateTodoDto } from './dto/create-todo.dto';

@ApiTags('todo')
@Controller('todo')
@UseInterceptors(new TransformInterceptor())
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Post()
  async createPost(@Body() todoCreateDto: CreateTodoDto) {
    return this.todoService.saveTodo(todoCreateDto);
  }

  @Get()
  async getTodo() {
    return this.todoService.getTodo();
  }
}
