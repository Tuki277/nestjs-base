import { Injectable } from '@nestjs/common';
import { TodoRepository } from './todo.repository';
import { CreateTodoDto } from './dto/create-todo.dto';
import { Todo } from './entities/todo.entity';

@Injectable()
export class TodoService {
  constructor(private todoRepository: TodoRepository) {}

  async saveTodo(req: CreateTodoDto): Promise<Todo> {
    req.status = 'WORKING';
    const todoData = this.todoRepository.create(req);
    return this.todoRepository.save(todoData);
  }

  async getTodo() {
    return await this.todoRepository
      .createQueryBuilder('todo')
      .innerJoinAndSelect('todo.user_id', 'user')
      .getMany();
  }
}
