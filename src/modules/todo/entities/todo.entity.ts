import { AppStatusTodo } from 'src/common/enum';
import { User } from 'src/modules/users/entities/user.entity';
import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'todo' })
export class Todo extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ length: 255 })
  title: string;

  @OneToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'user_id' })
  user_id: User;

  @CreateDateColumn()
  createdAt: Date;

  @Column({ name: 'due_date' })
  dueDate: Date;

  @Column({
    name: 'status',
    enum: AppStatusTodo,
    type: 'text',
    nullable: true,
  })
  status: string;
}
