import { IsDateString, IsNotEmpty, IsString } from 'class-validator';
import { User } from 'src/modules/users/entities/user.entity';
import { Column, DeepPartial } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

export class CreateTodoDto {
  @ApiProperty({ type: String })
  @IsNotEmpty()
  @IsString()
  title: string;

  @ApiProperty({ type: String })
  @IsNotEmpty()
  @IsString()
  user_id: DeepPartial<User>;

  @ApiProperty({ type: Date })
  @Column()
  @IsDateString()
  dueDate: Date;

  status: string;
}
